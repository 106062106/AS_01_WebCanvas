# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
主要參數－
（功能表上）
1.選色－
	使用input type="color"，滿足選色的功能。
2.線條寬度－
	使用input type="number"，可以由鍵盤輸入或點擊按鈕調整。
3.字型－
	使用input type="radio"，建立單選選項；並在點擊後觸發onclick，
	設定當前字型為serif、san-serif、monospace。
4.大小－
	使用input type="number"，可以由鍵盤輸入或點擊按鈕調整。
5.字串輸入－
	使用input type="text"，用於canvas打字功能。
（背景）
5.下筆狀態－
	True則mousemove可進行繪圖，false則禁止。
6.起點座標－
	繪圖時，下筆的起點座標。
7.工具代號－
	當前選擇的繪圖工具種類，若無則為0。
8.回復後畫面stack－
	用於儲存動作後canvas畫面。
9.重作後畫面stack－
	用於儲存回復前cnavas畫面。

功能種類－
1.鉛筆－
	ToolBox左上角鉛筆圖示，由onclick觸發；點擊後工具代號為1、更改canvas.cursor為［鉛筆圖示］。
	期間在canvas上mousedown後再mousemove則會依照選色、線條寬度繪圖；
	如果滑鼠點擊canvas以外區域，canvas.cursor回復為default，
	若維持mousedown離開canvas，可保持不需mousedown即可繪圖，再點擊後取消。
2.橡皮擦－
	ToolBox第一列左方第二之圖示，由onclick觸發；點擊後工具代號為2、更改canvas.cursor為［橡皮擦圖示］，
	並且更改context.strokeStyle為白色，之後利用鉛筆之繪圖函式進行繪圖。
3.畫圓－
	ToolBox第一列左方第三之圖式，由onclick觸發；點擊後工具代號為3、更改canvas.cursor為crosshair，
	並於放開滑鼠後，以最初mousedown、mouseup座標和context.arc畫圓。
4.畫長方形－
	ToolBox第一列左方第四之圖式，由onclick觸發；點擊後工具代號為4、更改canvas.cursor為crosshair，
	並於放開滑鼠後，以最初mousedown、mouseup座標和context.rect畫圓。
6.打字－
	ToolBox第二列左方第一之圖式，由onclick觸發；點擊後工具代號為6，更改canvas.cursor為［大寫字母Ｔ］，
	於canvas上點擊後，使用context.fillText將輸入之字串印於canvas。
	在canvas上進行拖曳，可以產生複數效果。
7.回復－
	ToolBox第二列左方第二之圖式，由onclick觸發；點擊後回復canvas之前一狀態。
	實作方式為在canvas剛載入以及mouseup時，以canvas.toDataURL將畫面狀態儲存，
	需要undo時，使用canvas.drawImage重新載入畫面。
8.重作－
	ToolBox第二列左方第三之圖式，由onclick觸發；點擊後重作回復前狀態。
	實作方式為將回復前，以canvas.toDataURL將畫面狀態儲存，
	需要redo時，使用canvas.drawImage重新載入畫面。
9.清除畫面－
	ToolBox第二列左方第四之圖式，由onclick觸發；點擊後利用context.clearRect清除畫面。