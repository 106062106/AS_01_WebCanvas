// Constants;
var bar_width = 250;
// Variables;
var drawing = false;
var old_cor = [0,0];
var font_tp = "Arial";
var tool_mode = 0;
var prev_act = [];
var next_act = [];
var canvas = document.getElementById("main");
var pencil = document.getElementById("pencil");
var color = document.getElementsByName("color")[0];
var thick = document.getElementsByName("thick")[0];
var size = document.getElementsByName("size")[0];

// Initialization Functions;
function set_main() {
	var target = document.getElementById("main");
	target.width = window.innerWidth - bar_width;
	target.height = window.innerHeight - 32;
}
function set_toolbar() {
	var target = document.getElementById("toolbar");
	target.width = bar_width;
	target.height = window.innerHeight - 32;
}
function set_font(font) {
	font_tp = font;
}
// Event Functions;
function pick_pencil() {
	tool_mode = 1;
	canvas.style.cursor = "url(./cursors/pencil.png), default";
}
function pick_eraser() {
	tool_mode = 2;
	canvas.style.cursor = "url(./cursors/eraser.png), default";
}
function pick_circle() {
	tool_mode = 3;
	canvas.style.cursor = "crosshair";
}
function pick_rect() {
	tool_mode = 4;
	canvas.style.cursor = "crosshair";
}
function pick_typer() {
	tool_mode = 6;
	canvas.style.cursor = "url(./cursors/text.png), default";
}
async function saveState() {
	await prev_act.push(canvas.toDataURL());
	next_act = [];
}
async function undo() {
	var context = canvas.getContext("2d");
	var num = prev_act.length;
	if (num) {
		var prev = prev_act[num - 1];
		prev_act.pop();
		await next_act.push(canvas.toDataURL());
		var img = new Image;
		img.src = prev;
		img.onload = async function() {
			var width = canvas.width;
			var height = canvas.height;
			context.clearRect(0, 0, width, height);
			await context.drawImage(img, 0, 0, width, height, 0, 0, width, height);
		}
	}
}
async function redo() {
	var context = canvas.getContext("2d");
	var num = next_act.length;
	if (num) {
		var next = next_act[num - 1];
		next_act.pop();
		await prev_act.push(canvas.toDataURL());
		var img = new Image;
		img.src = next;
		img.onload = async function() {
			var width = canvas.width;
			var height = canvas.height;
			context.clearRect(0, 0, width, height);
			await context.drawImage(img, 0, 0, width, height, 0, 0, width, height);
		}
	}
}
function clean() {
	saveState();
	var context = canvas.getContext("2d");
	tool_mode = 0;
	canvas.style.cursor = "default";
	context.clearRect(0, 0, canvas.width, canvas.height);
}
// Tool Functions;
function draw_line(new_cor) {
	var context = canvas.getContext("2d");
	context.beginPath();
	context.moveTo(old_cor[0], old_cor[1]);
	context.lineTo(new_cor[0], new_cor[1]);
	context.closePath();
	context.lineWidth = thick.value;
	context.stroke();
}
function draw_circle(new_cor) {
	var context = canvas.getContext("2d");
	var x = old_cor[0] - new_cor[0];
	var y = old_cor[1] - new_cor[1];
	var radius = Math.sqrt((x*x + y*y));
	context.beginPath();
	context.lineWidth = thick.value;
	context.arc(old_cor[0], old_cor[1], radius, 0, 2*Math.PI, true);
	context.stroke();	
}
function draw_rect(new_cor) {
	var context = canvas.getContext("2d");
	var lx, uy, width, height;
	context.beginPath();
	context.lineWidth = thick.value;
	if (old_cor[0] < new_cor[0]) {
		lx = old_cor[0]
		width = new_cor[0] - old_cor[0];
	} else {
		lx = new_cor[0];
		width = old_cor[0] - new_cor[0];
	}
	if (old_cor[1] < new_cor[1]) {
		ly = old_cor[1];
		height = new_cor[1] - old_cor[1];
	} else {
		ly = new_cor[1];
		height = old_cor[1] - new_cor[1];
	}
	context.rect(lx, ly, x, y);
	context.stroke();
}

// Initialization;
set_main();
set_toolbar();
// Canvas Event;
canvas.addEventListener("mousedown", function(event) {
	var context = canvas.getContext("2d");
	drawing = true;
	old_cor = [event.clientX, event.clientY];
	saveState();
});
canvas.addEventListener("mousemove", function(event) {
	event.preventDefault();
	event.stopPropagation();
	if (drawing) {
		var new_cor = [event.clientX, event.clientY];
		var context = canvas.getContext("2d");
		switch(tool_mode) {
			case 1:
				context.strokeStyle = color.value;
				draw_line(new_cor);
				old_cor = new_cor;
				break;
			case 2:
				context.strokeStyle = "white";
				draw_line(new_cor);
				old_cor = new_cor;
				break;
			case 6:
				var text = document.getElementsByName("text")[0].value;
				var args = "" + size.value + "px " + font_tp;
				context.font = args;
				context.fillText(text, new_cor[0], new_cor[1]);
				break;
			default:
				break;
		}
	}
});
canvas.addEventListener("mouseup", function(event) {
	var context = canvas.getContext("2d");
	var new_cor = [event.clientX, event.clientY];
	drawing = false;
	switch(tool_mode) {
		case 3:
			context.strokeStyle = color.value;
			draw_circle(new_cor);
			break;
		case 4:
			context.strokeStyle = color.value;
			draw_rect(new_cor);
			break;
		default:
			break;
	}
}, false);
window.addEventListener("mousedown", function(event) {
	if (!event.target.matches("#main")) {
		canvas.style.cursor = "default";
		tool_mode = 0;
	}
});